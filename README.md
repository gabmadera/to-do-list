# Completionist

A simple To-Do list createct using React as my first project using the library.

by Gabriel Madera Castell

## Description

Completionist is an easy to use to-do list web app.

You can add, favorite and delete tasks using the app, as well as checking your completed tasks and your favorites.

It has been created using React class components mainly. This was intended in order to develop my understanding of class components, and subsequently improving even more my understanding of Funcional Components when working with React.

As the project progresses, some key functionality will be added, such as task editing and profile management.

## Getting Started

### Installing

Inside the folder containing the package.json file, run npm install to install dependencies.

### Executing program

```
npm run start
```

## Authors

Gabriel Madera Castell
[@gabmadera](https://www.linkedin.com/in/gabriel-madera-castell/)

## Known bugs

When checked as favorite, icon doesn't appear to change. This is a work in progress.
