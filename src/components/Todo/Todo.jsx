import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

import MainList from '../MainList';
import Input from '../Input';

import './Todo.scss';

class Todo extends Component {
  state = {
    isEditing: false,
    showPopup: false,
  };

  handleSetEditing = () => {
    this.setState({
      isEditing: true,
      showPopup: !this.state.showPopup,
    });
  };

  handleStopEditing = () => {
    this.setState({
      isEditing: false,
      showPopup: !this.state.showPopup,
    });
  };

  render() {
    return (
      <div className="Todo">
        <h2 className="Todo__title">
          {this.props.title} ({this.props.numTasks})
        </h2>

        <div className="Todo__list">
          <MainList
            list={this.props.tasks}
            onCompleteTask={this.props.onCompleteTask}
            onStar={this.props.onStar}
            onRemoveTask={this.props.onRemoveTask}
          />

          {this.props.onCompleteTask && this.props.onAddTask ? (
            <React.Fragment>
              {this.state.isEditing ? (
                <Input
                  onAddTask={this.props.onAddTask}
                  onCancel={this.handleStopEditing}
                />
              ) : (
                <div className="Todo__end">
                  <button
                    className="Todo__end--add-button"
                    onClick={this.handleSetEditing}
                  >
                    <FontAwesomeIcon icon={faPlus} />
                  </button>
                  <span className="Todo__end--button-text">New Task</span>
                </div>
              )}
            </React.Fragment>
          ) : null}
        </div>
      </div>
    );
  }
}

Todo.propTypes = {
  title: PropTypes.string.isRequired,
  tasks: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      text: PropTypes.string,
    })
  ).isRequired,
  numTasks: PropTypes.number.isRequired,
  onCompleteTask: PropTypes.func,
  onAddTask: PropTypes.func,
  onStar: PropTypes.func,
  onRemoveTask: PropTypes.func,
};

Todo.defaultProps = {
  onCompleteTask: null,
  onAddTask: null,
  onStar: null,
  onRemoveTask: null,
};

export default Todo;
