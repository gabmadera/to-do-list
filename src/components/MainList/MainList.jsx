import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import Cards from '../Cards';

import './MainList.scss';

class MainList extends Component {
  render() {
    return (
      <ul className="MainList">
        {!this.props.list.length &&
        this.props.history.location.pathname === '/' ? (
          <h3 className="MainList__not-completed">
            You don't have any pending tasks!{' '}
          </h3>
        ) : null}

        {!this.props.list.length &&
        this.props.history.location.pathname === '/completed' ? (
          <h3 className="MainList__not-completed">
            Get to work! Finish some tasks!{' '}
          </h3>
        ) : null}

        {!this.props.list.length &&
        this.props.history.location.pathname === '/starred' ? (
          <h3 className="MainList__not-completed">
            Add some tasks to your favorites!{' '}
          </h3>
        ) : null}

        {this.props.list.map(({ id, description, title }) => (
          <React.Fragment key={id}>
            <Cards
              cardId={id}
              title={title}
              description={description}
              onCompleteTask={this.props.onCompleteTask}
              onStar={this.props.onStar}
              history={this.props.history.location.pathname}
              onRemoveTask={this.props.onRemoveTask}
            />
            {/* <hr /> */}
          </React.Fragment>
        ))}
      </ul>
    );
  }
}

MainList.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
      description: PropTypes.string,
    })
  ).isRequired,
  onCompleteTask: PropTypes.func,
  onStar: PropTypes.func,
  onRemoveTask: PropTypes.func,
  text: PropTypes.string,
};

MainList.defaultProps = {
  onCompleteTask: null,
  onStar: null,
  onRemoveTask: null,
};

export default withRouter(MainList);
