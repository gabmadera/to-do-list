import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';

import './Navbar.scss';

export default class Navbar extends Component {
  render() {
    return (
      <nav className="Navbar">
        <Link to="/" className="Navbar__link">
          {' '}
          <FontAwesomeIcon icon={faHome} />
        </Link>

        <div className="Navbar__end">
          <Link to="/completed" className="Navbar__link">
            Completed Tasks
          </Link>

          <Link to="/starred" className="Navbar__link">
            Favorites
          </Link>
        </div>
      </nav>
    );
  }
}
