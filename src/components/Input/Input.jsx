import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { uuid } from 'uuidv4';

import './Input.scss';

class Input extends Component {
  state = {
    title: '',
    description: '',
    showPopup: false,
  };

  handleOnChangeInput = (e) => {
    const { name, value } = e.target;

    this.setState({
      [name]: value,
    });
  };

  handleOnClickAdd = () => {
    const newTask = {
      title: this.state.title,
      description: this.state.description,
      id: uuid(),
    };

    this.setState({
      showPopup: !this.state.showPopup,
    });

    this.props.onAddTask(newTask);
    this.props.onCancel();
  };

  handleKeyPressed = (e) => {
    const { name, value } = e.target;
    if ((name === 'title' || 'description') && value !== '') {
      if (e.key === 'Enter' && value !== '') {
        this.handleOnClickAdd();
      }
    }
  };

  render() {
    return (
      <div className="Input">
        <div className="Input__inner">
          <h2 className="Input__inner--title">Add a new task</h2>
          <span className="Input__inner--quote">
            “Procrastination isn’t task management, it’s feeling management.”
          </span>
          <input
            name="title"
            type="text"
            value={this.state.title}
            onChange={this.handleOnChangeInput}
            placeholder="Task title"
            className="Input__inner--text-area"
            onKeyPress={this.handleKeyPressed}
          />
          <input
            name="description"
            type="text"
            value={this.state.description}
            onChange={this.handleOnChangeInput}
            placeholder="Task description"
            className="Input__inner--text-area"
            onKeyPress={this.handleKeyPressed}
          />
          <div className="Input__inner--buttons--2">
            <button
              onClick={this.handleOnClickAdd}
              disabled={!this.state.title}
              className="Input__inner--buttons--add"
            >
              Add Task
            </button>
            <button
              onClick={this.props.onCancel}
              className="Input__inner--buttons--cancel"
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    );
  }
}

Input.propTypes = {
  onAddTask: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};

export default Input;
