import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCircle,
  faCheckCircle,
  faStar,
} from '@fortawesome/free-regular-svg-icons';
import {
  faCheckCircle as faChecked,
  faStar as starChecked,
  faPen,
  faMinusCircle,
} from '@fortawesome/free-solid-svg-icons';

import './Cards.scss';

class Cards extends Component {
  state = {
    isHovering: false,
    isChecked: false,
    isStarHovering: false,
    isStarChecked: false,
  };

  handleOnClick = () => {
    const { onCompleteTask, cardId } = this.props;
    onCompleteTask(cardId);
  };

  handleOnRemove = () => {
    const { onRemoveTask, cardId } = this.props;
    onRemoveTask(cardId);
  };

  handleOnStar = () => {
    const { onStar, cardId } = this.props;
    onStar(cardId);
  };

  handleOnMouseEnter = (e) => {
    this.setState({
      isHovering: true,
    });
  };

  handleOnMouseLeave = (e) => {
    this.setState({
      isChecked: false,
      isHovering: false,
    });
  };

  handleOnMouseClick = (e) => {
    this.setState({
      isChecked: true,
    });
  };

  handleOnStarClick = (e) => {
    this.setState({
      isStarChecked: true,
    });
  };

  handleOnMouseUp = (e) => {
    this.setState({
      isChecked: false,
    });
  };

  handleOnStarUp = (e) => {
    this.setState({
      isStarChecked: false,
    });
  };

  handleOnStarHover = (e) => {
    this.setState({
      isStarHovering: true,
    });
  };

  handleOnStarLeave = (e) => {
    this.setState({
      isStarHovering: false,
      isStarChecked: false,
    });
  };

  render() {
    const { isHovering, isChecked, isStarHovering, isStarChecked } = this.state;
    const checkIcons = isChecked ? faChecked : faCircle;
    const starIcons = isStarChecked ? starChecked : faStar;

    return (
      <div>
        <li className="Card">
          {this.props.onCompleteTask ? (
            <button
              onClick={this.handleOnClick}
              onMouseEnter={this.handleOnMouseEnter}
              onMouseLeave={this.handleOnMouseLeave}
              onMouseDown={this.handleOnMouseClick}
              onMouseUp={this.handleOnMouseUp}
              className="Card__button"
            >
              <FontAwesomeIcon
                icon={isHovering && !isChecked ? faCheckCircle : checkIcons}
              />
            </button>
          ) : (
            <span className="Card__button">
              <FontAwesomeIcon icon={faChecked} />
            </span>
          )}
          <div className="Card__task">
            <h4 className="Card__title">{this.props.title}</h4>
            <p className="Card__text">{this.props.description}</p>
          </div>

          <div className="Card__options">
            {this.props.onStar ? (
              <button
                className="Card__star"
                onClick={this.handleOnStar}
                onMouseEnter={this.handleOnStarHover}
                onMouseLeave={this.handleOnStarLeave}
                onMouseDown={this.handleOnStarClick}
                onMouseUp={this.handleOnStarUp}
              >
                <FontAwesomeIcon
                  icon={
                    isStarHovering && !isStarChecked ? starChecked : starIcons
                  }
                />
              </button>
            ) : (
              <button className="Card__star">
                <FontAwesomeIcon
                  icon={
                    isStarHovering && isStarChecked ? starIcons : starChecked
                  }
                />
              </button>
            )}
            {this.props.history !== '/completed' &&
            this.props.history !== '/starred' ? (
              <React.Fragment>
                <button className="Card__edit">
                  <FontAwesomeIcon icon={faPen} />
                </button>

                <button className="Card__edit" onClick={this.handleOnRemove}>
                  <FontAwesomeIcon icon={faMinusCircle} />
                </button>
              </React.Fragment>
            ) : null}
          </div>
        </li>
      </div>
    );
  }
}

Cards.propTypes = {
  cardId: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  onCompleteTask: PropTypes.func,
  onStar: PropTypes.func,
  history: PropTypes.string,
  onRemoveTask: PropTypes.func,
};

Cards.defaultProps = {
  onCompleteTask: null,
  onStar: null,
  onRemoveTask: null,
};

export default Cards;
