import low from 'lowdb';
import LocalStorage from 'lowdb/adapters/LocalStorage';

const adapter = new LocalStorage('db');
const db = low(adapter);

// Añadimos valires por defecto a nuestra nueva DB
db.defaults({ tasks: [], completed: [], starred: [] }).write();

export default db;
