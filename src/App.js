import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import db from './api/db';

import Navbar from './components/Navbar';
import Todo from './components/Todo';

import './App.scss';

class App extends React.Component {
  state = {
    tasks: [
      {
        title: '',
        description: '',
      },
    ],
    completed: [],
    starred: [],
  };

  async componentDidMount() {
    const dbState = await db.getState();
    this.setState(dbState);
  }

  handleAddTask = async (task) => {
    const newTask = db.get('tasks').push(task).write();
    this.setState({
      tasks: newTask,
    });
  };

  handleRemoveTask = async (id) => {
    await db.get('tasks').remove({ id }).write();
    const newTasks = await db.get('tasks').value();

    this.setState({
      tasks: newTasks,
    });
  };

  handleAddCompleted = (task) => {
    const newCompleted = db.get('completed').push(task).write();

    this.setState({
      completed: newCompleted,
    });
  };

  handleCompleteTask = (id) => {
    const newTask = this.state.tasks.find((task) => task.id === id);

    this.handleRemoveTask(id);
    this.handleAddCompleted(newTask);
  };

  handleStar = (taskId) => {
    const task = this.state.tasks.find((el) => el.id === taskId);

    if (!this.state.starred.includes(task)) {
      const newStar = db.get('starred').push(task).write();

      this.setState({
        starred: newStar,
      });
    }
  };

  render() {
    console.log(this.state);

    return (
      <Router>
        <div className="App">
          <Navbar />

          <Switch>
            <Route
              path="/completed"
              render={() => (
                <Todo
                  tasks={this.state.completed}
                  title="Your completed tasks"
                  numTasks={this.state.completed.length}
                />
              )}
            />
            <Route
              path="/starred"
              render={() => (
                <Todo
                  tasks={this.state.starred}
                  title="Your Favorite Tasks"
                  numTasks={this.state.starred.length}
                />
              )}
            />

            <Route
              path="/"
              render={() => (
                <Todo
                  title="Your pending tasks"
                  numTasks={this.state.tasks.length}
                  tasks={this.state.tasks}
                  onCompleteTask={this.handleCompleteTask}
                  onStar={this.handleStar}
                  onAddTask={this.handleAddTask}
                  onRemoveTask={this.handleRemoveTask}
                />
              )}
            />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
